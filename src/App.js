import React, { Component } from 'react';
import SearchHeader from './components/SearchHeader';
import ImgList from './components/ImgList';
import axios from 'axios';
import _ from 'lodash';
import $ from 'jquery';
import './App.css';

class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      initialLoaded: true,
      photos: [],
      query : 'naruto wallpaper',
      feedPageNo: 0,
      noMorePages: false
    };

  }

  getImages = () => {

   let selfRef = this;
    selfRef.setState({
      isLoaded: false
    });

    if(!selfRef.state.noMorePages){
      axios.get("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=a5d5dd15732a48688e62c85389f6a4e5&tags="+ this.state.query + "&per_page=10&page="+(this.state.feedPageNo+1)+"&format=json&nojsoncallback=1")
      .then(function (response) {
        selfRef.setState({
          isLoaded: true,
          initialLoaded : false,
          feedPageNo : response.data.photos.page,
          photos: _.concat(selfRef.state.photos,response.data.photos.photo)
        });
        if(response.data.photos.page >= response.data.photos.pages) {
            selfRef.setState({
              noMorePages : true
            })
        }
      })
      .catch(function (error) {
        console.log( error )
        selfRef.setState({
          isLoaded: true
        });
      })
      .then(function () {
        // always executed
      });
    }
  }

  componentDidMount() {
    let scrollableEle = $(window);
    this.getImages();
    window.addEventListener('scroll', this.onScroll.bind(this), true);
    // scrollableEle.on("scroll", this.onScroll.bind(this));
  }

  componentWillUnMount() {

  }


  onScroll() {
      let scrollableEle = $(window);
      let selfRef = this;

      var scrollHeight = $(document).height(),
          scrollPosition = $(window).height() + $(window).scrollTop();

      if ((scrollHeight - scrollPosition) <= 200 && selfRef.state.isLoaded) {
         this.getImages();
      }
  }

  getImagesByQuery(arg){
    this.setState({
      isLoaded: false,
      query: arg?arg:"naruto wallpaper",
      feedPageNo: 0,
      photos: [],
      noMorePages: false
    });
    console.log(arg);
    this.getImages();
  }



  render() {
    return (
      <div className="App">
        <SearchHeader searchImg={   _.debounce( (arg)=> { this.getImagesByQuery(arg) }, 1000 )  } />

        { this.state.initialLoaded ? '' : <ImgList photos={this.state.photos} /> }
        
        { this.state.isLoaded ? '' : <p className="loading">loading ...</p> }
      </div>
    );
  }
}

export default App;
