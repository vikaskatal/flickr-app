import React, { Component } from 'react';

class SearchHeader extends Component {

  render() {
    return (
      <header className="App-header">
        <h2>Search Photos</h2>
        <input onChange={(e) => this.props.searchImg(e.target.value)} placeholder="Search..." type="text" />
      </header>
    );
  }
}

export default SearchHeader;
