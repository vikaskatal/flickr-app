import React, { Component } from 'react';

class ImgList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      seletedImage: {}
    };
  }

  photoPopup(imgObj) {
       this.setState({
         popup : true,
         seletedImage : imgObj
       });
  }

  hidePhotoPopup(){
    this.setState({
      popup : false,
      seletedImage : {}
    });
  }

  render() {

    const photos = this.props.photos;

    let photoMap = photos.map((photo, index) =>
          <div className="item" key={'photo-'+index} onClick={ () => this.photoPopup(photo) }>
            <img
            src={ "https://farm"+ photo.farm +".static.flickr.com/"+photo.server+"/"+photo.id+"_"+photo.secret+"_b.jpg "}
            title={photo.title}/>
          </div>
      );

    return (
      <div className="list">
        {
          this.state.popup ?
            <div className="popup">
              <div className="close-btn" onClick={() => this.hidePhotoPopup()}><span>✕</span></div>

              <div className="popup-content">
                  <div className="img">
                    <img
                    src={ "https://farm"+ this.state.seletedImage.farm +".static.flickr.com/"+this.state.seletedImage.server+"/"+this.state.seletedImage.id+"_"+this.state.seletedImage.secret+"_b.jpg "}
                    title={this.state.seletedImage.title}/>
                  </div>
                  <p className="title">{this.state.seletedImage.title}</p>
              </div>


            </div>
           : ""
        }
        {photoMap}
      </div>
    );
  }
}

export default ImgList;
